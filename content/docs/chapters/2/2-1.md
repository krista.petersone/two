---
title: "Straupes lauku labumu tirdziņš"
weight: 21

author: "Linda Cīrule"
categories: "piemēri"
tags: [""]
---


# Straupes lauku labumu tirdziņš 

## Situācija
Tirgi ir iecienītas pārtikas iegādes vietas Latvijas patērētāju vidū, jo tā ir iespēja iegādāties autentiskus ražojumus. Tomēr, lai zemnieks regulāri varētu pārdot savu produkciju, viņam jāplāno savs laiks ceļam uz Rīgu vai citu pilsētu, kur tirgi notiek regulāri. Lauku teritorijā tirgi tiek rīkoti galvenokārt uz svētkiem (Līgo, Miķeļi, Ziemassvētki u.c.), un gandrīz visi ir pārbagāti ar importētiem pārtikas produktiem un rūpnieciskām precēm. Straupes tirgus rīkotāji vēlējās radīt autentisku un regulāru tirdzniecības vietu tuvu vietējiem ražotājiem un patērētājiem, kas ļautu ietaupīt laiku un resursus, kā arī veicinātu ražotāju sadarbību un viņu saiknes ar vietējiem patērētājiem.  

## Risinājums
Lai radītu vietu, kur satikties vietējiem ražotājiem un pircējiem, un samazinātu pārtikas transportēšanas ģeogrāfisko un arī sociālo attālumu starp vietējiem ražotājiem un patērētājiem, Straupē tika izveidots regulārs tirgus. Straupes ciemats, kurā dzīvo aptuveni 1500 iedzīvotāju, atrodas izdevīgā vietā pie autoceļa A3 Inčukalns—Valmiera—Igaunijas robeža (Valka), kas tirgu padara viegli pieejamu arī nejaušiem, garām braucošiem klientiem.  Tādējādi tajā īstenojas alternatīva prakse: nevis lauksaimnieks ceļo no laukiem uz pilsētu, bet pircēji no pilsētas uz laukiem. Lai nostiprinātu vietējo ražotāju situāciju tirgū, tika izstrādāts tirgus darbības nolikums, kurš paredz, ka tirgū priekšroka tiek dota ražotājiem-pārdevējiem no vietējās teritorijas (līdz 30 km). Ja vietējie tirgotāji nevar piedāvāt konkrēto produktu, to var piedāvāt tirgotāji ne vairāk kā 100 km attālumā. Veidojot tirgus identitāti, tika aktualizēts pārtikas ilgstpējīgas ražošanas un izplatīšanas devums kopējam sabiedrības labumam un piesaistītas sabiedrībā atpazīstamas personības, kuras novērtē vietējā produkta unikalitāti: gan ražošanas apstākļus, kas ietekmē kvalitāti, gan personīgo attieksmi.  

### Iesaistītie dalībnieki
Dažādos tirgus attīstības posmos ir bijis iesaistīts plašs dalībnieku loks. Visupirms vietējie aktīvisti, kopienas līderi identificēja vietējā tirgus veidošanas vajadzību, pulcēja līdzīgi domājošos un brīvprātīgi uzņēmās idejas īstenošanu. Būtisks bija vietējās pašvaldības stratēģisks atbalsts tirgus rīkošanai. Vietējā pienotava atbalstīja tirgus rīkošanu savā privātīpašumā. Iniciatīvu atbalstīja un tai pieslējās vairāki vietējie lauksaimnieki - ražotāji, kuri piekrita definētajiem tirgus noteikumiem. Tika nodibināta saikne ar vietējo tūrisma asociāciju, un Tūrisma un informācijas centrs atbalstīja mārketinga aktivitātes un izplatīja informāciju. Valsts institūcijas - Pārtikas un veterinārais dienests un Valsts Ieņēmumu dienests - pretimnākoši izskaidroja tirgus rīkošanai saistošo normatīvo aktu bāzi. Lai mainītu normatīvo aktu regulējumu nacionālā mērogā, tika aktualizēta sadarbība ar lauksaimnieku organizācijām, kuras ietekmē politikas veidošanas procesus. Tirgus identitātes nostiprināšanais tika veidota sadabība ar Slow food kustības dalībniekiem un mārketinga ekspertiem. Visbeidzot, tirgus sekmīga īstenošanā būtiski bija vietējie pircēji, kā arī pircēji no attālākām vietām, kuri bija motivēti mērot ceļu, lai iepirktos autentiskā vietējo ražotāju tirdziņā.

### Resursi
Straupes tirgus apliecina iedzīvotāju iniciatīvas un sadarbības lomu, kas kopā ar cilvēkresursu - iniciatoru zināšanām un starptautisko pieredzi par līdzīgu aktivitāti citās Eiropas valstīs - bija nozīmīgākie resursi tirgus uzsākšanā. Tirgus iniciatoru kopiena nodibināja biedrību “Vesels pilsētā un laukos”, un biedrības juridiskais statuss ļāva piesaistīt ES fondu finanšu līdzekļus. Tirgus ierīkošanai ir nepieciešama fiziska vieta, un būtisks bija vietējās pienotavas atbalsts, ļaujot to rīkot savā īpašumā.

Attēls ...

## Rezultāts
Ir izveidots kopienas virzīts, regulārs vietējo produktu tirdziņš. Lielākā daļa pārdevēju ir vietējie un reģionālie mazie un vidējie lauksaimnieki un amatnieki. Ir arī vietējie iedzīvotāji, kuri pārdod produkciju, kas ražota piemājas saimniecībās, kā arī tiek tirgoti savvaļas produkti (sēnes, ogas, ziedi). Ir arī daži vietējie vai reģionālie pārtikas uzņēmumi, kas piedalās tirgū, jo ne visās tirgus dienās ir pietiekami daudz vietējo ražotāju, kas reģistrējas pārdošanai. 

## Ietekme
Straupes tirgus aktīvā darbība veicinājusi mājražotāju un amatnieku darbību vietējā teritorijā, līdz ar to arī mazo ģimenes uzņēmumu attīstību, nodrošinot viņu piekļuvi tirgum, palielinot ienākumus un rosinot inovācijas. Tirgus īstenošanas gaitā tika mainīti Ministru Kabineta noteikumi, kas iepriekš ierobežoja zemnieku tirgu darbību, nosakot, ka tie drīkstēja tikt rīkoti ne vairāk kā astoņas reizes gadā. Mainot noteikumus, tika pavērtas plašākas iespējas vietējo tirgu rīkotājiem. Tirgus organizēšanas gaitā ražotāji tika izglītoti par pārtikas ražošanas un izplatīšanas noteikumiem un nodokļiem. Tāpat arī veicināta sabiedrības izpratne par resursu ilgtspējīgu izmantošanu un to nozīmi lauku teritorijas sociālajā un ekonomiskajā vidē. Vietējai lauku telpai tika piešķirta jauna vērtība, izceļot produktu vietējo izcelsmi, vietējo produktu kvalitāti un ražotāju personību. Vietējā tirgus izveide stiprinājusi vietējo ražotāju un patērētāju saiknes un mazinājusi pārtikas kilometrus - ražotājiem un patērētājiem veicamo ceļu līdz tālākām tirdzniecības vietām. Turklāt no Straupes tirgus piemēra iedvesmojas arī citas kopienas, kuras vēlas veidot savu vietējo tirdziņu, un Straupes tirgus organizētāju kopiena ir atsaucīga dalīties pieredzē un zināšanās, kā to darīt. 

## Secinājumi un ieteikumi
Ko nevar viens, to var vairāki. Tirgus iniciatīva ir partnerība, kurā ir sadarbojies plašs dalībnieku loks, lai to īstenotu. Šāda plaša sadarbība ir nepieciešama, lai nodrošinātu tirgus īstenošanai visus nepieciešamos resursus (iniciatīva, zināšanas, finansējums, vieta u.c.). Kopumā Straupes lauku labumu tirdziņš ir piemērs, kā kopdarbība kopienā var rosināt un ieviest pārmaiņas pārtikas sistēmā. Tas uzlabojis vietējo kontroli pār pārtikas ražošanu, tirdzniecību un patēriņu. Tirgus iniciatīva apliecina (sociālās) mācīšanās un inovācijas (organizācija, mārketings, ražošana) nozīmi lauku attīstībā. Tam ir sinerģija ar vietējo attīstību: sociālās dzīves un tradīciju (lauksaimnieku tirgus un kultūra) aktivizēšanu, ieguldījumu vietējā ekonomikā un tūrismā.

## Avots
Foodlinks: Skat. Šūmane, S. Pp. 66-69:
https://www.foodlinkscommunity.net/fileadmin/documents_organicresearch/foodlinks/CoPs/evidence-document-sfsc-cop.pdf 
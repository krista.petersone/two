---
title: "Tiešā tirdzniecība"
weight: 20

bookCollapseSection: true

author: ["Sandra Šūmane", "Anda Ādamsone-Fiskoviča"]
categories: "apraksti"
tags: [""]
---

# Lauksaimnieciskās produkcijas tiešā tirdzniecība

## Situācija
Pārtikas apritē dominē garas pārtikas ķēdes, kurās lauksaimnieku ražotais līdz patērētājam nonāk caur daudziem starpniekiem, un lielveikali ir izplatītākais iedzīvotāju iepirkšanās kanāls. Garās pārtikas ķēdes ļāvušas efektīvi apgādāt patērētājus ar produktiem lielos apjomos. Taču tajās valdošie efektivātes un standartu kritēriji – ražot daudz, pēc iespējas lētus un noteiktiem standartiem atbilstošus produktus – ierobežo to lauksaimnieku izredzes iesaistīties šajos aprites kanālos, kuru ražošanas prakses, apjomi un produkti atšķiras. Vienlaikus, aktualizējoties jautājumiem par pārtikas sistēmas ilgtspēju, pieaug sabiedrības izpratne par pārtikas izcelsmi, sastāvu un ietekmi uz veselību, garu sagādes ķēžu negatīvo ietekmi uz vidi un lauksaimnieku situāciju pārtikas sistēmā. Veidojas pieprasījums pēc pārtikas caurspīdīgākas aprites, un pārtikas sistēmas dalībnieki meklē ilgtspējīgākus pārtikas aprites risinājumus. 

## Risinājums 
Tiešā tirdzniecība ir alternatīvs risinājums, ko izmanto daļa lauksaimnieku, paši pārdodot savus produktus gala patērētājam bez starpniekiem. Tādējādi, veidojas tiešājas, ciešākas  individuālas  attiecības starp ražotāju un patērētāju. Pastāv virkne dažādu lauksaimnieciskās produkcijas tiešās tirdzniecības risinājumu: tirdzniecība saimniecībās (tostarp saimniecību veikali, pašapkalpošanās stendi, pašlasīšanas dārzi u. c.), zemnieku tirgos, piegāde klientiem mājās, birojos, kā arī tiešās pirkšanas pulciņiem. Informācijas un komunikācijas rīku attīstība ļauj izmantot digitālā mārketinga iespējas un veidot dažādas tiešsaistes tirdzniecības vietas: pirkšanas platformas, saimniecību mājas lapas, lapas sociālajos tīklos u. c. Tādējādi tiešā tirdzniecība, lai arī ierasta prakse daudzu lauksaimnieku vidū, nepārtraukti attīstās, apvienojot pārbaudītas zināšanas ar jaunām. Tā ietver plašu tirgus, organizatorisko un tehnoloģisko inovāciju spektru, kā arī e-komercijas, loģistikas un citu pakalpojumu izmantošanu.

![demo](/images/2-1.png) ![demo](/images/2-2.png)


### Iesaistītie dalībnieki
Tiešā tirdzniecība ir būtisks produkcijas noieta kanāls tām mazām un vidējām saimniecībām, kam ir sarežģītāk piekļūt konvencionālajiem tirgus kanāliem. Patērētāju pieprasījums pēc vietējiem, svaigiem, tradicionāliem produktiem ir būtisks tiešās tirdzniecības attīstībai, un nereti iniciatīva nāk no viņiem. Citi pārtikas sistēmas dalībnieki var sniegt nozīmīgu atbalstu tiešās tirdzniecības ieviešanai. Piemēram, Straupes tirgus izveidi rosināja vietējās kopienas pārtāvji, un tas tika īstenots ar pašvaldības iesasiti un vietējā uzņēmuma atbalstu, ļaujot tirgu rīkoti savā teritorijā.

### Resursi
Tiešās tirdzniecības sekmīgums ir lielā mērā atkarīgs ne tikai no lauksaimnieka tirdzniecības zināšanām un iemaņām, bet arī saimniekošanas pamatā esošajā agrotehniskajām zināšanām, izpratni par ekonomiku un tirgus principiem, grāmatvedības un juridiskajiem jautājumiem, kā arī no valodu prasmēm un darba pašorganizācijas spējām. Lai arī būtiskas ir formālās, konsultāciju organizāciju sniegtās zināšanas par plašākiem lauksaimniecības un saimniecības vadības jautājumiem, liela nozīme ir neformāliem zināšanu apguves veidiem - paša pieredzei vai konsultācijām ar radiniekiem vai citiem lauksaimniekiem. Atkarībā no tiešās tirdzniecības veida var būt nepieciešama fiziskas vai virtuālas infrastruktūras izveide: piemēram, tirgus placis ar nepieciešamo aprīkojumu, veikala iekārtošana saimniecībā, saimniecības mājaslapas izstrāde u. c.

## Rezultāts
Tiešā tirdzniecība nodrošina lauksaimnieku piekļuvi tirgum, sniedzot iespēju gūt augstākus ienākumus. Nereti tiešās saiknes ar patērētāju izvēršas noturīgās, uzticībā un abpusējā cieņā balstītās attiecībās, nodrošinot lauksaimniekam pastāvīgu klientu loku. Attiecības ar patērētājiem rosina inovācijas, jo ražotājs tiešā veidā uzzina par patērētāju vēlmēm un gūst idejas jauniem produktiem, kas var vēl vairāk nostiprināt viņa tirgus pozīciju. Patērētājiem tiešā tirdzniecība uzlabo piekļuvi svaigiem, vietējiem, tradicionāliem un nišas produktiem, kas nav pieejami konvencionālajos pārtikas sagādes kanālos. Tiešā tirdzniecība nostiprina vietēja un reģionāla mēroga pārtikas sistēmas, kurās vietējiem pārtikas sistēmas dalībniekiem ir lielāka pašnoteikšanās. Vienlaicīgi jāmin, ka šis produkcijas realizācijas veids var būt ražotājiem krietni laikietilpīgs, nereti paredz lielu fizisku un emocionālu slodzi, kā arī prasa būtiskus saimniecības ierastās darbības organizācijas pielāgojumus. 

## Secinājumi un ieteikumi
Tiešā tirdzniecība var būt ekonomiski izdevīga un sabiedrības atbalstu apliecinoša tirgus piekļuves alternatīva lauksaimniekiem, it sevišķi tiem, kuru ražošanas prakses, apjomi un produkti atšķiras un kuriem ir sarežģīti pārdot savu produkciju konvencionālajās pārtikas ķēdēs. Nostiprinot lauksaimnieku ekonomisko pozīciju, tiešajai tirdzniecībai ir pozitīva ietekme uz lauku ekonomiku kopumā. Veiksmīgi tiešās tirdzniecības piemēri, atbilstoši materiālie apstākļi un infrastruktūra, maziem un vidējiem ražotājiem piemērots pārtikas aprites regulējums, sociālo tīklu izmantošana un tiešās tirdzniecības zināšanas un pieredze ir būtiski faktori tiešās tirdzniecības veicināšanai un atjaunināšanai īsajās pārtikas sagādes ķēdēs. 

## Avoti
- AgriLink PA32:Formālu un neformālu konsultāciju loma tiešajā tirdzniecībā Pierīgas reģionā 
- Ādamsone-Fiskoviča, A., Grīviņš, M., Ķīlis, E., Tisenkopfs, T., Ozola, L., Orste, L., Krūmiņa, A., Šūmane, S. (2020) Latvijas iedzīvotāju ēšanas paradumi un attieksme pret dažādiem ar uzturu saistītiem jautājumiem. Ziņojums par iedzīvotāju aptaujas rezultātiem. Rīga: Baltic Studies Centre
- Šūmane, S. (2018) Snapshot: Expressions of Urban - Peri-Urban - Rural Relationships Tukums food market. https://rural-urban.eu/sites/default/files/S-TUK3%20Tukums%20Food%20Market.pdf 
- Šūmane, S., Kilis, E., Tisenkopfs, T., Adamsone-Fiskovica, A., Grivins, M. (2019) Latgale (RR14) - Latvia - Food System Regional Report. SALSA. http://www.bscresearch.lv/uploads/files/RR14_Latgale_FS%20Regional%20Report_final_no%20KI.pdf 
- Šūmane, S., Adamsone-Fiskovica, A., Tisenkopfs, T., Grivins, M. (2019) Pierīga (RR 15) - Latvia - Food System Regional Report. SALSA. http://www.bscresearch.lv/uploads/files/RR15_Pieriga_FS%20Regional%20Report_final_no%20KI.pdf 


## Piemēri 

{{<section>}}

---
title: "Digitalizācija lauku attīstībā"
weight: 70

bookCollapseSection: true

author: "Tālis Tisenkopfs"
categories: "apraksti"
tags: [""]
---


# Digitalizācijas principi un pieejas lauku attīstībā

## Situācija 
Zemnieku saimniecībās un lauku uzņēmumos arvien vairāk tiek ieviestas digitālās tehnoloģijas – sensori, datu bāzes, precīzās metodes, virtuālās ierīces, autonomās sistēmas, satelītdati, mobilās aplikācijas, roboti, uc. Šīs tehnoloģijas ļauj uzņēmējiem efektīvāk izmantot resursus, kāpināt produktivitāti, aizstāt roku darbu, uzlabot mārketingu, biznesa vadību. Eiropas Savienības Kopējā lauksaimniecības politika digitalizāciju virza kā vienu no nākotnes prioritātēm līdzās Zaļajam kursam. Taču digitalizācijas radītās iespējas nav vienādi pieejamas visiem lauku uzņēmējiem un iedzīvotājiem. Digitālās tehnoloģijas var radīt arī sociālas un ekonomiskas plaisas. Lai digitalizācija būtu ilgtspējīga, proti – veicinātu gan uzņēmumu konkurētspēju, gan uzlabotu iedzīvotāju labklājību, sekmētu vides aizsardzību un citus svarīgus lauku dzīves aspektus, digitālās tehnoloģijas nepieciešams papildināt ar atbalstošiem sociāliem un politiskiem risinājumiem. 

## Risinājums
Apvārsnis 2020 projekts DESIRA liecina, ka lauksaimniecības attīstību līdz 2040. gadam būtiski ietekmēs tādas digitālās tehnoloģijas kā precīzā lauksaimniecība un datu kombinēta izmantošana.  Mežsaimniecībā nozīmīgs būs meža resursu digitāls monitorings un lielo datu apstrāde. Lauku attīstības jomā  perspektīvi digitālie risinājumi tiek saistīti ar publisko pakalpojumu nodrošināšanu digitālajā vidē, attālinātām darba formām un viediem teritoriju attīstības risinājumiem (piemēram – viedie ciemi). 

**Galvenie aģenti**, kas virza lauku digitalizāciju ir: lauku uzņēmēji, lauksaimniecības tehnikas ražotāji un piegādātāji un informācijas un komunikācijas pakalpojumu kompānijas, piemēram – mobilo sakaru operatori. Šie aģenti pamatā mijiedarbojas tirgus ietvaros. Ieviešot nozarei būtiskas digitālās platformas (piemēram, Latvijā – Lauku atbalsta dienesta informatīvās sistēmas), aktīva loma ir valstij un publiskā sektora organizācijām. Digitālajās iniciatīvās, kas vērstas uz sabiedrības vajadzībām un teritoriju integrētu attīstību, bieži kopdarbojas visi četrkāršās spirāles dalībnieki – industrija, pētnieki, pārvaldes iestādes, pilsoniskās sabiedrības grupas. 

## Rezultāts
DESIRA projektā digitalizācija tiek pētīta kā sabiedrisks, politisks un tehnisks process. Digitālas tehnoloģijas veido ilgtspējīgu lauksaimniecības un lauku nākotni, ja tiek ņemtas vērā, ne tikai biznesa intereses, bet arī sabiedrības vajadzības un intereses. Sekmīgi lauku digitalizācijas piemēri liecina, ka ieguldījumi digitālajā infrastruktūrā un tehnoloģijās ir tikuši papildināti ar izglītojošiem pasākumiem, cilvēku digitālo prasmju pilnveidošanu. Kā piemēru var minēt Eiropas digitālo nedēļu, bet dalībvalstu līmenī – Francijā pieņemto Digitālās iekļaušanas plānu.

## Secinājumi un ieteikumi
DESIRA projekts ir sagatavojis septiņas rekomendācijas digitalizācijas pārvaldībai lauku reģionos:
- Radīt digitalizācijai nepieciešamo infrastruktūru. 
- Saistīt digitalizāciju ar ilgtspējīgu attīstību.
- Pielāgot digitalizāciju kontekstam, iesaistot ieinteresētās puses un vietējos iedzīvotājus. 
- Atbalstīt digitālo iekļaušanu, lai izvairītos no marginalizācijas un polarizācijas.
- Attīstīt digitālās ekosistēmas, īpaši atbalsot padomdevējus un zināšanu brokerus.
- Veidot elastīgu digitālo pārvaldību, kas ļauj mijiedarboties politiķiem, zinātniekiem, praktiķiem. 
- Veidot ilgtspējīgu digitalizāciju veicinošus politikas instrumentus, kas atbalstītu vietējās kopienas. 

## Piemēri
{{<section>}}


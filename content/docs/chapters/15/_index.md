---
title: "Lauksaimnieku kooperatīvi"
weight: 150
#bookCollapseSection: true

author: "Miķelis Grīviņš"
categories: "apraksti"
tags: [""]
---

# Lauksaimnieku kooperatīvi

## Situācija
Lauksaimnieki ikdienā saskaras ar virkni strukturālu un institucionālu izaicinājumu, piemēram, zema produktu iepirkuma cena, dārga ražošanas tehnika un agroķīmija, grūtības ieineresēt banku piedāvāt lauksaimnieka vajadzībām pielāgotu aizdevumu u.c. Lielās saimniecības var būt pietiekoši ietekmīgs sadarbības partneris aprites ķēdēs, lai ietekmētu augstāk minētos faktorus. Tomēr biežāk atsevišķām saimniecībām pieejamie resursi nav pietiekami, lai ietekmētu sagādes ķēdē dominējošās prakses: lauksaimniekiem nav pietiekoši laika, zināšanu, sociālo tīklu un instrumentu, lai aizstāvētu savas intereses un mainītu sadarbības partneru uzvedību sev labvēlīgākā virzienā. Apvienojoties lielākās grupās - kooperatīvos, lauksaimnieki var iegūt resursus, kuri tiem pietrūkst individuāli. Katrs sedzot tikai daļu no izmaksām, kooperatīva biedri var kopā piesaistīt speciālistus, kas uzkrāj zināšanas, veidot kontaktu tīklus un kompetenti iesaistīties sarunās ar sadarbības partneriem. Kopā tirgojot savu produkciju lauksaimnieki palielina savu nozīmi sagādes ķēdē, palielinot partneru interesi par lauksaimnieku grupu.

## Risinājums
Kooperācija tiek uzlūkota kā daudzsološs risinājums praktiski visiem izaicinājumiem, ar ko saskaras lauksaimnieki. Divi sektori, kuri Latvijā visbiežāk tiek pieminēti kooperācjas kontekstā ir piena lopkopība un graudkopība. SUFISA lauksaimnieku aptaujas dati rāda, ka ~44% graudkopju un ~34% piena lopkoju ir kooperatīva biedri, un vidēji ~31,22% no kopējās graudu produkcijas un ~34,87% no kopējās piena produkcijas tiek realizēta caur kooperatīviem. Abos sektoros kooperatīvi visbiežāk pilda  lauksaimnieku produkcijas iepirkšanas funkciju: šādu atbildi ir norādījuši ~94% piena lopkopībā  un ~84% graudkopībā strādājošie kooperatīvu biedri. Otrā biežāk minētā kooperatīvu biedru (~36% graudkopju un ~37% piena lopkopju) atbilde ir, ka kopperatīvi palīdz lauksaimniekiem vienoties ar pircēju. Retāk kooperatīvu biedri norāda, ka  kooperatīvi palīdz izstrādāt līgumus ar produkcijas pircējiem (abos sektoros ~17% respondentu) un nodibināt kontaktus ar pircēju (~12% graudkopju un ~14% piena lopkopju). Starp graudkopjiem ~29% kooperatīvu biedru norāda, ka kooperatīvi sniedz viņiem arī citus pakalpojumus palīdz piekļūt kredītiem, agroķīmijai, nodrošina agronoma konsultācijas, veicina zināšanu apriti, utt.
Šie dati ilustrē, ka kooperatīvu nozīmīgākā loma Latvijā ir veidot lauksaimnieka saikni ar tirgu, vienlaikus strādājot pie lauksaimnieka interešu aizstāvības. Pateicoties lielākam produkcijas apjomam, ko ražotāju grupas var pircējam piedāvāt, pircējs par produkciju ir gatavs maksāt augstāku cenu. Praksē piemēri rāda, ka kooperatīvi funkcionē stipri atšķirīgi. Kā ilustrē graudkopju pieredze, kooperatīvi var pildīt daudz nozīmīgāku lomu, piedāvājot saviem biedriem plašāku pakalpojumu klāstu par izdevīgākām cenām. Latvijā vairāki kooperatīvi ir iesaistījušies arī lauksaimniecības produkcijas pārstrādē. 

### Resursi
Kooperatīviem nepieciešamie finanšu resursi ir saistīti ar uzdevumiem, ko kooperējoties lauksaimnieki risina. Piemēram, kopīgi pārstrādājot, uzglabājot vai pārvietojot produkciju, kooperatīvam var būt nepieciešams iegādāties inventāru, kas ļauj minētās funkcijas izpildīt un šajā gaījumā kooperatīvam būs jāpiesaista finansējums. Kooperatīvi var dažādi piesaistīt investīcijas darba uzsākšanai - finansējums var nākt no biedriem, publiskā atbalsta instrumentiem vai investoriem. Vienlaikus kooperatīva funkcionēšanā nozīmīgi ir arī nefinansiāli resursi - Latvijā vairākkārt ir pierādījies, ka koperatīvu veiksme ir atkarīga no biedru attieksmes un gatavības uzņemties ilgtermiņa saistības ar kooperatīvu.  

## Rezultāts
Veiksmīgi kooperatīvi mazina lauksaimnieku atkarību no citiem sagādes ķēdes dalībniekiem (pārstrādātājiem, tirgotājiem u.c.) un rada iespējas individuālos lauksaimnieka izaicinājumus risināt kopīgi ar zemākiem individuāliem ieguldījumiem.
Secinājumi un ieteikumi: Kooperatīvi var tikt izmantoti, lai risinātu virkni dažādu izaicinājumu, ar kuriem lauksaimnieki ikdienā saskaras. Kooperācijas potenciāls ir praktiski neizsmeļams. Kooperatīva vadībā vēlams piesaistīt profesionāļus, kuri paši nav lauksaimnieki.

## Avoti
- Grivins, M., Tisenkopfs, T., Adamsone-Fiskovica, A. and Sumane, S. (2019). Latvia National report (WP 2 - Deliverable 2.2). Pieejams te: https://www.sufisa.eu/wp-content/uploads/2018/09/D_2.2-Latvia-National-Report.pdf

---
title: "Pārstrāde mazajās saimniecībās"
weight: 30

bookCollapseSection: true

author: ["Marija‌ ‌Stefānija‌ ‌Skudra‌","Tālis Tisenkopfs"]
categories: "apraksti"
tags: [""]
---

# Pārstrāde mazajās saimniecībās

## Situācija
Mājas apstākļos ražotu pārtikas produktu ražošana tiek organizēta atbilstoši Pārtikas un veterinārā dienesta noteiktajām pārtikas drošības prasībām. Līdz 2014. gadam likumdošana paredzēja, ka mājražotāji savu produkciju drīkst izplatīt tikai gala patērētājam, nedodot iespēju tos realizēt lieliem pārtikas preču izplatītājiem. Eiropas Parlamenta un Padomes regulas mājās ražotām precēm un grozījumi Pārtikas aprites uzraudzības likumā paredz prasību atvieglošanu atbalstot saražoto produktu mazumtirdzniecību un atvieglojot nosacījumus ražošanas infrastruktūrai.

## Risinājums
Pārstrāde mazajās saimniecībās ir labs risinājums saimniecību ienākumu kāpināšanai, saražotās produkcijas izmantošanai ar lielāku pievienoto vērtību, vietējās produkcijas attīstībai un kulināro tradīciju saglabāšanai. Mazie pārtikas uzņēmumi pārstrādei parasti izmanto augkopības, lopkopības, piensaimniecības, zvejas, medus un citus produktus, un izejvielas. Izplatīti produkcijas veidi ir sulas, ievārījumi, konservējumi, medus izstrādājumi, kūpinājumi, sieri. Pārstrāde bieži tiek apvienota ar lauku tūrisma aktivitātēm. Saimniecības nodarbojas arī ar produkcijas pārdošanu. Galvenie tirdzniecības kanāli ir tiešā tirdzniecība, zemnieku tirgi, izbraukuma pasākumi un lauksaimniecības izstādes. Ražotāji izmanto arī digitālos rīkus, pārdodot produktus tiešsaistē gala patērētājiem, piemēram,  tiešās pirkšanas pulciņiem. Ražošanā tiek izmantotas vēsturiskās un vietējās zināšanas, veidota atgriezeniskā saikne ar patērētāju. Pastāv izteikta saikne starp mājražotājiem un pilsētas patērētājiem, kuri ir galvenā klientu grupa. 

### Resursi
Produktu ražošanā tiek izmantoti gan savā, gan citu zemnieku saimniecībās iegūtie resursi. Latvijas Lauku konsultāciju un izglītības centrs nodrošina zināšanu ieguves pakalpojumus - seminārus, konsultācijas un kursus lauksaimniekiem pārstrādes un pārdošanas jautājumos. Finansiāls atbalsts tiek sniegts apakšpasākuma "Atbalsts jaunu produktu, metožu, procesu un tehnoloģiju izstrādei" (MK noteikumi Nr. 222, 16.2) ietvaros, atbalstot jaunu produktu un tehnoloģiju ieviešanu. Atbalsta intensitāte tiek piemērota konkursa kārtībā 90 % apmērā no attiecināmajām izmaksām. 

### Iesaistītie dalībnieki
Zemnieki mazajās saimniecībās ir galvenie pārstrādes attīstītāji. Zemnieki var sadarboties ar citām mājsaimniecībām papildu izejvielu iegūšanai. Lauku atbalsta dienests sniedz finanšu atbalstu iekārtu iegādei. Vietējās attīstības un pārvaldes iestādes cenšas veicināt vietējās produkcijas ražošanu, pārstrādi un patēriņu, organizējot mārketinga atbalsta pasākumus, piemēram - pilsētu svētkus, lauku produktu dienas, veicina pārtikas zīmološanu (piemēram, 0 km iniciatīva Alentejo reģionā, “Ražots Madonas novadā”). Reģionālā līmenī darbojas biedrības, kas atbalsta kulinārā mantojuma un tradīciju saglabāšanu, piemēram, biedrība “Latgales kulinārā mantojuma centrs” Eiropas kulinārā mantojuma tīkla ietvaros.

## Rezultāts 
Projektu rezultātā tiek uzlabota piekļuve vietējiem produktiem, tiek turpinātas kulinārās tradīcijas, paplašināts vietējo produktu klāsts un tie darīti pieejamāki patērētājiem. Mazo pārtikas uzņēmumu stiprās puses galvenokārt ir saistītas ar to iekļaušanos vietējā kontekstā un pieaugošais pieprasījums pēc vietējiem un bioloģiski ražotiem produktiem. Nomaļa atrašanās vieta tālu no reģionālajiem centriem, kā arī nepietiekams tehniskais aprīkojums var kavēt mazo pārstrādes uzņēmumu attīstību. Iedzīvotāju skaita samazināšanās lauku apvidos apgrūtina darbaspēka pieejamību. Taču augstākas kvalitātes preču piedāvājums uzlabo mazo pārstrādātāju konkurētspēju salīdzinājumā ar masu produktiem.

## Secinājumi
Atbalstot pārstrādi mazajās saimniecībās, tiek panākta zemnieku saimniecību nostriprināšanās. 

## Avoti
- Ministru kabineta noteikumi Nr. 222. (13.05.2017). Valsts un Eiropas Savienības atbalsta piešķiršanas kārtība 16. pasākuma "Sadarbība" 16.1. apakšpasākumam "Atbalsts Eiropas Inovāciju partnerības lauksaimniecības ražīgumam un ilgtspējai lauksaimniecības ražīguma un ilgtspējas darba grupu projektu īstenošanai" un 16.2. apakšpasākumam "Atbalsts jaunu produktu, metožu, procesu un tehnoloģiju izstrādei" atklātu projektu iesniegumu konkursu veidā. Izgūts no https://likumi.lv/ta/id/290651-valsts-un-eiropas-savienibas-atbalsta-pieskirsanas-kartiba-16-nbsppasakuma-sadarbiba-16-1-apakspasakumam-atbalsts-eiropas
- SALSA Latgale (RR14) Latvia Food System Regional Report

## Piemēri

{{<section>}}

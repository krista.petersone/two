---
title: "Publiskais iepirkums"
weight: 50

bookCollapseSection: true

author: "Miķelis Grīviņš"
categories: "piemēri"
tags: [""]
---

# Publisks ilgtspējīgas pārtikas iepirkums

## Situācija
Pārtikas sistēmu var mēģināt mainīt motivējot atsevišķus uzņēmumus mainīties, vai ieviešot regulējumu, kas uzreiz ļauj mainīt sistēmu kopumā. Vienlaikus, centralizēta regulējuma pieeja nevar sasniegt vēlamo rezultātu, jo pārtikas sistēmu gadījumā konteksts nozīmīgi ietekmē kādas pārmaiņas ir nepieciešams - identiskas pārmaiņas dažādos kontekstos var novest pie kardināli pretējiem rezultātiem. Rezultātā, starp pārvaldei pieejamajiem instrumentiem ir jāmeklē tādi, kas var motivēt lokālā līmenī iesaistītās puses pašas vienoties par labākajiem veidiem kā strukturēt partikas sistēmas. Vienlaikus nacionālā līmenī ir nepieciešams tikai nosaukt principus, kuriem vietējām pārtikas sistēmām vajadzētu sekot.

## Risinājums 
Viens no daudzsološākajiem risinājumiem pārtikas sistēmu ilgtspējīgu pārmaiņu veicināšanai ir ilgtspējīgas pārtikas iepirkumus. Katrā konkrētajā gadījumā tas, kā tieši tiek skaidrots, kas ir ilgtspīgs pārtikas iepirkums, atšķirsies. Tomēr kopumā šāds iepirkums paredz fokusu uz vietēju, bioloģisku, veselīgi un nereti arī sociāli atbildīgu un ētiski ražotu pārtiku. Parasti iepirkuma apraksta izstrādes gaitā iepirkuma organizētājs izdala atsevišķas galvenās pārtikas produktus raksturojošas kategorijas, kas konkrētajā vidē ir īpaši svarīgas. Ilgtspējīga pārtikas iepirkuma pieeja paredz, ka izmantojot pārvaldei nepieciešamo materiālu iepirkumu, var radīt tirgu jauna veida produktiem, tādējādi atbalstot sektora prakšu maiņu pārvaldes iecerētajā virzienā. Šī pieeja strādā, jo, no vienas puses, pārvaldes sektoram nepieciešamais (iepirktais) produktu apjoms parasti ir pietiekami liels, lai radītu pārtikas ražotājos interesi mainīties. No otras puses, publiskais iepirkums rada priekšnosacījumus, lai noteiktu produktu ražošana kļūtu lētāka un attiecīgi šie produkti kļūtu pieejamāki arī plašākai sabiedrībai. Pārtikas iepirkuma gadījumā pārvalde koncentrējas uz valsts vai pašvaldības uzturētām iestādēm - skolām, slimnīcām, bērnu dārziem, cietumiem un citām publiskām institūcijām, kurās tiek nodrošināta ēdināšana. Praksē, lai iepirkums varētu tikt izmantots ilgtspējas mērķu sasniegšanai, tā ieviešanā ir jāievēro vairāki principi. Pirmkārt, tā sekmīgums nevar tikt nodrošināts tikai ar regulējumu - ir nepieciešams iesaistīto pārvaldes pārstāvu entuziasms un izpratne/ atbalsts iepirkuma mērķiem. Iepirkuma ieviesējiem ir jābūt zināšanām par vietējo pārtikas sistēmas struktūru (kas un ko ražo un kādas ir katra ražotāja iespējas nodrošināt loģistiku. Otrkārt, lai arī nacionāla līmeņa politiski stimuli ir būtiski, IPI iespējams efektīvi izmantot tikai vietējā mērogā, kritiski izvērtējot vēlamo iepirkuma aptverto teritoriju. Treškārt, ir kritiski jāizvērtē kādi produkti un kādās kombinācijās tiek iepirkti. 

### Iesaistītie dalībnieki
Starptautiskā prakse rāda, ka sekmīga uz ilgtspēju vērsta iepirkuma ieviešana prasa plašu dažādu dalībnieku iesaisti. Primāri, protams, ir nepieciešams politikas veidotāju atbalsts - gan nacionālā, gan arī vietējā līmenī. Lai salāgotu dažādus mērķus un novērtētu prioritātes ir nepieciešams sadarboties dažādiem pārvaldes līmeņiem un jomām. Piem., iepirkuma gadījumā var būt situācijas, kurās ir nepieciešama Izglītības un zinātnes ministrijas, Veselības ministrijas, Lauksaimniecības ministrijas u.c. institūciju dialogs. Nozīmīga ir vietējās pašpārvaldes klātbūtne - tikai vietējie spēlētāji spēs praktiski novērtēt katras teritorijas iespējas, stiprās puses un riskus. Tāpat, tikai pašas institūcijas, kas nodrošina ēdināšanu (piem., skolas), spēs novērtēt savu kapacitāti kontrolēt un uzturēt iepirkuma rezultātā radušās jaunās prakses. Šī iemesla dēļ sekmīgos iepirkumos ir piesaistītas arī nevalstiskās organizācijas - gan vecāku grupas, vides organizācijas, ražotāju grupas, utt. Starp iesaistītajiem ir jāmin arī uzņēmēji, kuri piegādā produktus. Visu beidzot, te var pieminēt arī pētnieku lomu, kuri meklē veidus kā nodrošināt tādu informācijas apriti starp iesaistītajām pusēm, kas varētu palīdzēt pilnībā izmantot katrā teritorijā pieejamos resursus.

### Resursi
No finanšu resursiem vienīgie, kurus nepieciešams pieminēt ir līdzekļi, kurus pārvalde atvēl iepirkumam. Prakse gan rāda, ka finanšu resursi nav primārais faktors, kas nosaka ilgtspējīga iepirkuma sekmīgumu - to labāk skaidro iesaistīto dalībnieku izpratne par iepirkuma mērķiem, dialogs starp iesaistītajām pusēm, zināšanas par iepirkumu un par vietējo situāciju.

## Rezultāti un ietekme 
Tiešais ilgtspējīga pārtikas iepirkuma rezultāts ir ilgtspējīgas maltītes publiskajās iestādēs. Šis ir galvenais ilgtspējīga iepirkuma mērķis daudzām iesaistītājām grupām (vecākiem, skolēniem, nereti arī iesaistītajiem uzņēmējiem). Tomēr, iltspējīgs pārtikas iepirkums ir jāaplūko arī plašāk - pievēršot uzmanību iepirkuma vēlamajai ietekmei uz sektoru un prakšu maiņu tajā. 
Secinājumi: Ilgtspējīgs pārtikas iepirkums ir daudzsološs instruments kā mainīt pārtikas sistēmās dominējošās prakses. Diemžēl, tā ieviešana ir komplicēta - to var ieviest tikai lokāli, tikai speciālisti ar labām zināšanām par vietējām pārtikas sistēmām un tikai iesaistot vietējās pārtikas sistēmas pārstāvjus. Neskatoties uz šiem izaicinājumiem, pasaulē ir virkne piemēru, kur ilgtspējīgs iepirkums ir spējis mainīt pārtikas sistēmas.

## Avoti 
Grivins, M., Kunda, I., Tisenkopfs, T. (2016). Healthy food for schoolgoers in Latvia and Small farm involvement in school meal provisioning in Tukums region. 
Sonnino, R. (2009). Quality Food, Public Procurement, and Sustainable Development: The School Meal Revolution in Rome. Environment and Planning A: Economy and Space. Doi: https://doi.org/10.1068/a40112

## Piemēri 
{{<section>}}
---
title: "Savvaļas produktu komercializācija "
weight: 120

bookCollapseSection: true

author: "Miķelis Grīviņš"
categories: "apraksti"
tags: [""]
---

# Savvaļas produktu komercializācija 

## Situācija
Meklējot veidus, kā risināt daudzveidīgos ar ilgtspēju saistītos izaicinājumus, attāli lauku reģioni nereti ir grūtas izvēles priekšā. Šajās teritorijās daba - gan kultivēta, gan nekultivēta, ir tuvāk, kā tā būtu pilsētā. Latvijas gadījumā šādu apgalvojumu lieliski ilustrē tas, ka vairāk nekā puse no valsts teritorijas ir mežaina. Attiecīgi dabas un ekosistēmu “saglabāšanu” kopumā biežāk saista tieši ar šādām teritorijām. Vienlaikus, salīdzinot ar Rīgu, Pierīgu vai citām salīdzinoši blīvi apdzīvotām Latvijas teritorijām, attālo lauku teritoriju iedzīvotāju sociālās un ekonomiskās iespējas ir nozīmīgi zemākas (gan skatoties uz mājsaimniecību ienākumu atšķirībām, gan arī uz nodarbinātības iespējām). Izaicinājumi visās (vides, sociālās un ekonomiskās) ilgtspējas dimensijās rada grūtības atrast piemērotus risinājumus, kas vienlaicīgi būtu vērsti gan uz vides saglabāšanu, gan uz iedzīvotāju vajadzību apmierināšanu. Šajās teritorijās nereti arī trūkst pieredzes un resursu, lai ieviestu sarežģītus citur strādājošus risinājumus.

## Risinājums
Latvijā savvaļas produktu vākšana ir plaši izplatīta. Starp vācējiem ir tādi, kuri veiksmīgi gūst peļņu no savvaļas produktiem. Iespēja komercializēt šos produktus ir plaši aplūkota starptautiskā literatūrā vairāku iemeslu dēļ. Pirmkārt, valstīs, kurās iedzīvotājiem ir neierobežota piekļuve savvaļas produktiem, šie produkti garantē iespējas no tiem pelnīt visiem - tas ir resurss, kas ir pieejams ikvienam un kurš ļauj iedzīvotājiem pašiem nodrošināt sev nepieciešamos ienākumus. Otrkārt, pārdomāti organizējot savvaļas produktu vākšanu, to komercializācija var kalpot par tiltu, kas savieno ilgtspējīgas vides apsaimniekošanas prasības ar šībrīža sabiedrības materiālajām vajadzībām. Treškārt, vākšana ir viegli apvienojama ar citiem ienākumu avotiem, kā, piemēram, lauku tūrisms, mazās saimniecības uzturēšana utt. 
Savvaļas produktu komercializācijas modeļi var būt dažādi, bet, plaši lūkojoties, var izdalīt četrus galvenos: (1) savvaļas produktu intensifikācija (fokuss uz šauru produktu loku, bet uz lielu produktu apjomu), (2) ierobežota savvaļas produktu tirdzniecība (fokuss uz ļoti dažādu savvaļas produktu komercializāciju, galvenokārt pelnot tieši no šo produktu tirdzniecības), (3) savvaļas produktu kā dzīvesstila pakalpojumu tirdzniecība (fokuss uz plašu produktu klāstu, bet tie tiek tirgoti nelielos apjomos; galvenā peļņa uzņēmumam veidojas no darbībām, kas pavada darbu ar savvaļas produktiem) un (4) savvaļas produktu pastarpināts izmantojums citu produktu vai pakalpojumu mārketinga nolūkos (neliela savvaļas produktu dažādība un neliels šo produktu apjoms; savvaļa galvenokārt tiek izmantota, lai veidotu patērētāja uztveri). Šajos četros modeļos stipri atšķiras stratēģijas, kādas uzņēmēji izmanto. Tomēr tās vieno digitālo rīku nozīmība, lai uzrunātu un veidotu attiecības ar klientu, pilsētas kā produktu galvenie noieta tirgi un uzņēmumu devums pārtikas ķēdēs cirkulējošās veselīgas un ilgtspējigas pārtikas apjoma un sortimenta papildināšanā.

### Iesaistītie dalībnieki
Savvaļas produktus un ar tiem saistītos pakalpojumus tirgū piedāvājošie  uzņēmumi ir savvaļas produktu komercializācijas centrā. Šie uzņēmumi var būt saistīti ar citu jomu uzņēmējiem. Piemēram, praksē sastopami veiksmīgi piemēri sadarbībai starp savvaļas medicīnas augu vācējiem un biškopjiem. Kopumā šie savvaļas produktu nišā darbojošies uzņēmumi veiksmīgi paši nodrošina savu dzīvotspēju. Vienlaikus šī specifiskā uzņēmējdarbības joma pēdējos gados ir arī piesaistījusi zinātnieku uzmanību, kuri pēta to, kā saistīt savvaļas produktu izmantošanu ar citiem saimniekošanas modeļiem. Tāpat savvaļas augu vākšana un vākšanas ietekme uz vidi ir piesaistījusi arī vides organizāciju interesi. Strap grupām, kuras ir ieintresētas savvaļas produktu vākšanā, ir jāmin arī to mežu un pļavu, kuros aug savvaļas produkti, īpašnieki.

### Resursi
Savvaļas produktu komercializācijā nepieciešamie resursi nozīmīgi atšķiras starp dažādām komercializācijas stratēģijām. Lai piekļūtu pašiem savvaļas produktiem, īpaši finansiāli ieguldījumi nav nepieciešami. Tas ļauj šos produktus izmantot kā drošības spilvenu - no tiem  var gūt labumu sabiedrības grupas, kurām būtu ierobežotas citas iespējas (riska grupas, kas dzīvo attālās lauku teritorijās). Arī savvaļas produktu tirdzniecībā tās vienkāršākās formās ieguldījumi nav nepieciešami. Tajā pašā laikā pat nelielā uzņēmumā, kurš tiecas norošināt sev stabilu ienākumu avotu, ir nepieciešamas ievērojamas zināšanas par savvaļas produktiem un mārketingu. Šie uzņēmumi arī nereti iepērk vienkāršas pārstrādes iekārtas, kas ļauj palielināt produkta vērtību. Uzņēmumi, kuri intensificē savvaļas produktus, piesaista investīcijas, lai iegādātos jau sarežģītākas šķirošanas un pārstrādes līnijas. Attiecīgi intensificēt savvaļas produktus var būt dārgi.

## Rezultāts
Savvaļas produkti palīdz nodrošināt papildu vai pat pamatienākumu avotu iedzīvotājiem attālos lauku reģionos. Šis risinājums lielākoties ir dabai draudzīga alternatīva vidi ekspluatējošiem uzņēmējdarbības virzieniem un var tikt izmantots kombinācijā ar citiem līdzīgiem uzņēmējarbības veidiem (vienlaikus ir jāņem vērā, ka pārāk aktīva savvaļas produktu vākšana var būt vidi degraējoša). Jāuzsver arī ar savvaļas produktu komercializāciju saistītās uzņēmējdarbības informatīvā un izglītojošā loma - ir iemesls domāt, ka atsevišķi biznesa modeļi ar tajos īstenotajām darbībām var uzlabot sabiedrības izpratni par ekosistēmu kopsakarībām.

## Secinājumi un ieteikumi
Savvaļas produkti var būt nozīmīgs ienākumu avots. Tos pārdomāti komercializējot, ir iespējams nonākt pie uzņēmējdarbības modeļa, kas vērsts uz vides, ekonomiskās un sociālās ilgtspējas salāgošanu un veicināšanu. Apvienojot savvaļas produktus ar lauksaimniecību, var nonākt pie inovatīviem uzņēmējdarbības modeļiem.

## Avoti
- Grivins, M. 2021. Are All Foragers the Same? Towards a Classification of Foragers. Sociologia Ruralis.
- Grivins, M. 2020. Forging by foraging: The role of wild products in shaping new relations with nature. In: Routledge Handbook of Sustainable and Regenerative Food Systems, J. Duncan, M. Carolan and J.S.C. Wiskerke (eds.). 277-288.
- Grivins, M., Tisenkopfs, T. 2018. Benefitting from the global, protecting the local: The nested markets of wild product trade. Journal of Rural Studies.

## Piemēri
{{<section>}}
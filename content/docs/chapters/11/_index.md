---
title: "Brikolāža"
weight: 110 

bookCollapseSection: true

author: "Miķelis Grīviņš"
categories: "apraksti"
tags: [""]
---

# Brikolāža - izmantojot pieejamos resursus 

## Situācija
Par pārmaiņām parasti tiek runāts kā par lineāru procesu - kāds iecer uzlabojumu, izstrādā plānu, kā līdz vēlamajam rezultātam nokļūt, piesaista investīcijas un pakāpeniski izpilda plānu. Šāda notikumu secība gan dzīvē ir reti novērojama un drīzāk attēlo, kā mēs stāstām par pārmaiņām pēc tam, kad tās jau ir ieviestas. Praksē nokļūšana pie mērķiem ir krietni haotiskāka. Pārmaiņas pēc būtības reprezentē jauno, un kā tādas tās ne vienmēr ir veiksmīgi pielāgotas esošajai sistēmai - vairumam cilvēku vieglāk ir darīt lietas ierastajā veidā, un tādēļ atbalsts pārmaiņām var būt stirpi ierobežots. Tāpat, lai gan mums gribētos ticēt, ka mēs spējam iztēloties jaunu risinājumu sekas vai trajektorijas, pa kurām mēs varam līdz jaunajiem rezultātiem nokļūt, ir liela iespēja, ka mēs neesam pilnībā novērtējuši pārmaiņu dažādus aspektus. Rezultātā pārmaiņas vēl to ieviešanas mirklī var nebūt kārtīgi saplānotas, to ieviešanas procesā var izrādīties krietni vairāk izaicinājumu, un arī finansējums nereti tām var izrādīties nepietiekošs. Visu beidzot, arī paši pārmaiņu mērķi laika gaitā var krietni mainīties. Saskaroties ar šiem izaicinājumiem, var izrādīties, ka pārmaiņām nav nepieciešamā atbalsta, lai iegūtu papildu resursus izaicinājumu pārvarēšanai. Tā vietā pārmaiņu ieviesējiem ir jāiztiek ar sev jau pieejamajiem materiāliem. 

## Risinājums
Gadījumos, kad pārmaiņas neiegūst nepieciešamo finansējumu, kas varētu pilnībā nosegt visus pārmaiņu ieviešanas soļus, pārmaiņu ieviesējs (parasti organizācija) var izmantot brikolāžas pieeju. Šāda no apakšas nākušu pārmaiņu ieviešana savā būtībā ir paļaušanās uz sev pieejamajiem resursiem, cerot, ka tos īstajā brīdī varēs pielāgot konkrēto izaicinājumu risināšanai. Šādu ļoti praktisku materiālu pielāgošanu sauc par brikolāžu, un tā ir pieeja problēmu risināšanai, kuras ietvaros galvenais fokuss ir uz šībrīža vajadzībām un aptuveno vēlamo progresa virzienu (akcentējot “aptuveno”). Visvieglāk brikolāža ir iztēlojama, skatoties uz risinājumiem vienkāršām ikdienas problēmām - piemēram, mirklī, kad gribas ierīkot jaunu puķu dobi, bet pietrūkst rīku, lai to izveidotu, var paņemt pa rokai esošu kasti vai riepu un to izmantot par puķu dobi. Vienlaikus brikolāžai ir gana daudz piemēru arī organizāciju sadarbības līmenī - piemēram, mirklī, kad kādam uzņēmumam X nepietika līdzekļu, lai apkoptu savu dārzu, tas vienojās ar vietējo botānistu pulciņu, kuri sāka rīkot nodarbības dārzā, reizē arī apkopjot to. Attiecīgi apkārtējo resursu pielāgošana var tikt izmantota individuālā un arī organizāciju līmenī, un tā var tikt vērsta, lai sasniegtu individuālus, vai arī plašākus organizāciju mērķus. Kāds, kurš brikolē, izmanto resursus, kas ir pa rokai, ir atvērts pret to pielietojumu, ir gatavs pievērt acis uz nelielām nobīdēm un fokusējas uz virzību mērķa virzienā, nevis uz pašu mērķi.

Lai brikolāža izdotos, ir nepieciešams būt **atvērtam pret resursiem, kas organizācijai ir pieejami**. Šāds apgalvojums ietver ļoti daudz dažādas nozīmes - runa ir gan par fiziskiem resursiem, kurus var izmantot, risinot ļoti tehniskus izaicinājumus, gan arī par juridiskiem un sociāliem resursiem. Piemēram, organizācija var pārdefinēt sev pieejamo telpu pielietojumu vai būt elastīga pret darbinieku darba pienākumiem. Brikolāža arī paredz, ka organizācija uzkrāj sev pieejamos resursus. Atkal tas no vienas puses var nozīmēt, nevis mest laukā, bet noglabāt praktiskas lietas. Tomēr tas arī nozīmē veidot attiecību tīklu vai, piemēram, veidot dažādus attiecību modeļus. Citiem vārdiem, šis te otrais punkts paredz atvērtību pret lietām, kuras nav uzreiz nepieciešamas ar pieņēmumu, ka nākotnē tās būs noderīgas. Organizācijām, kuras iesaistās brikolāžā, ir jābūt jūtīgām pret procesa gaitu. Ieviestiem risinājumiem ir tieksme nostabilizēties un kļūt par tipisku veidu, kā lietas tiek darītas. Attiecīgi organizācijai būs izvēle saglabāt izstrādāto risinājumu par tipiso veidu, kas turpmāk tiks replicēts, vai turpināt uzlabot procesus ar sev pieejamajiem resursiem. Visu beidzot, brikolētājam ir jabūt atvēram pret iegūto rezultātu. Dažādu pa rokai esošu materiālu salikums nenesīs to rezultātu, kādu organizācija sākumā ir iecerējusi. Lai veicinātu pārmaiņas, organizācijai ir jābūt atvērtai pret iegūto rezultātu.

### Iesaistītie dalībnieki
Brikolāžas ietvaros īsti nevar runāt par dalībniekiem. Svarīgāk ir sarpast, kā tiek domāts par apkārt esošajām organizācijām, indivīdiem un indivīdu grupām. Šie potenciālie dalībnieki var iesaistīties un atbalstīt pārmaiņu ieviešanu. Svarīgi ir atrast sadarbības modeli, kas veicinātu pārmaiņas un būtu interesants arī apkārtējiem, kuri potenciāli varētu kļūt par dalībniekiem.
### Resursi
Arī te ir jānorāda, ka brikolāžas ietvaros jēdziens resursi tiek interpretēts citādi - tas galvenokārt ir domāšanas veids un atvērtība eksperimentēt ar dažādiem pieejamo materiālu pielietojumiem. Svarīgais ir no esošajiem resursiem dabūt objektu, kas ir tuvākais nepieciešamajam. 

## Rezultāts
Brikolāža ir veids, kā domāt par pārmaiņu ieviešanu. Brikolāžas rezultātā vajadzētu tikt ieviestai pārmaiņu formai, kura ir salīdzinoši stabili iesakņojusies sociālajā vidē, bet nenosedz visus izaicinājumus, kurus sākotnējās pārmaiņas paredzēja risināt. Brikolāža ir pastāvīgs process.
Secinājumi un ieteikumi: Pārmaiņu ieviešana prasa atvērtību domāšanā un šāda atvērtība var novest pie inovatīviem risinājumiem.

## Avoti
- Grivins, M., Keech, D., Kunda, I. un Tisenkopfs, T. 2021. Bricolage for Self-Sufficiency: An Analysis of Alternative Food Networks. Sociologia Ruralis. DOI: 10.1111/soru.12171

## Piemēri
{{<section>}}

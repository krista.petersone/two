---
title: "Pilsētu pārtikas stratēģijas"
weight: 40

bookCollapseSection: true

author: "Tālis Tisenkopfs"
categories: "apraksti"
tags: [""]
---

# Pilsētu pārtikas stratēģijas

## Situācija
Mūsdienu urbanizētās sabiedrības nodrošināšana ar pārtiku notiek, lielākoties pateicoties industrializētām un globalizētām pārtikas apgādes ķēdēm. Taču tām piemīt būtiski trūkumi ilgtspējas ziņā – industriālās pārtikas sistēmas rada ievērojamu vides piesārņojumu, koncentrē ienākumus lielražotāju rokās, izspiež no aprites mazos ražotājus. Šajā situācijā pilsētas arvien vairāk apzinās savu lomu ilgtspējīgu pārtikas sistēmu veidošanā. 

**Definīcija** 
PPS ir politikas dokumenti un rīcību kopums ar mērķi veicināt ilgtspējīgas pārtikas sistēmas teritoriālā līmenī (pilsētas, pašvaldības, reģioni). Ar PPS palīdzību pilsētas apzinās savu pārtikas sistēmu un plāno tajā ilgtspējīgas pārmaiņas. (FOODLINKS projekts).

## Risinājums
Lai meklētu alternatīvus teritoriālus risinājumus pārtikas apgādē, kopš 2000. gadu sākuma daudzas pilsētas pasaulē veido savas pārtikas stratēģijas (PPS). Monreāla, Roma, Barselona, Londona ir bijušas starp pionieriem. 2015. gadā tika nodibināta starptautiska pilsētu kustība “Milānas Pilsētu pārtikas politikas pakts”, kurā līdz šim iesaistījušās vairāk kā 200 pilsētu, tostarp Rīga.  PPS piedāvā plašu risinājumu spektru pārtikas sistēmu uzlabošanai. Malme (Zviedrija) savā pārtikas stratēģijā izvirzījusi mērķi 2020. gadā publiskajā iepirkumā skolu vajadzībām iegādāties tikai bioloģiski ražotu pārtiku un panākt, lai iepirkuma radītās siltumnīcas gāzu emisijas būtu par 40% zemākas nekā 2002. gadā. Bristolē (Apvienotā Karaliste 	) ir izveidota īpaša Pārtikas politikas padome, kura izstrādā pilsētas un reģiona pārtikas noturības plānu. Bāzeles (Šveice) pārtikas stratēģija fokusējas uz etniski un kulturāli atšķirīgu cilvēku saliedēšanu kopīgās dārzkopības aktivitātēs pilsētvidē. Vitorija-Gasteiza (Spānija) uzsvaru liek uz īso pārtikas ķēžu veicināšanu un zemnieku tirdziņu attīstību. Ārvalstu pieredze liecina, ka PPS virzieni var būt dažādi: uzlabot iedzīvotāju piekļuvi veselīgiem produktiem, veicināt vietējas pārtikas ražošanu un patēriņu, izglītot iedzīvotājus par veselīgu uzturu, uc. PPS iniciatori parasti ir pētnieki, pilsoniskās grupas un vietējie politiķi. Kopumā, PPS materializējas gan kā politikas dokumenti, gan rīcību kopums ar mērķi veicināt ilgtspējīgas pārtikas sistēmas teritoriālā līmenī (pilsētas, pašvaldības, reģioni).

Attēls...

### Iesaistītie dalībnieki
PPS izstrādē un ieviešanā vērojama visu zināšanu un inovāciju partneru iesaiste, bet atšķirīgā intensitātē dažādos stratēģijas posmos. Pilsoniskās sabiedrības rosinātu stratēģiju iniciēšanā (Bāzele, Bristole) kopienas aktīvisti un pētnieki uzņemas vadošo lomu. Pašvaldību rosinātu stratēģiju gadījumā (Malme) ierēdņi ir galvenie virzītāji, taču aktīvi iesaista arī citus partnerus (skolu pavāri, pārtikas piegādātāji, u.c.). Vitorijā-Gasteizā PPS aizsāka lauksaimniecības konsultanti sadarbībā ar zemnieku organizācijām, tālāk veidojot saites ar pilsētas patērētājiem un pārvaldes iestādēm. Pārtikas industrija (pārstrādes uzņēmumi, tirgotāji, ēdinātāji) ir relatīvi pasīvāks dalībnieks un parasti nav starp PPS iniciatoriem, taču var iesaistīties vēlākos posmos praktisku pasākumu īstenošanā.

### Resursi
PPS izstrāde un ieviešana prasa dažādu resursu mobilizāciju un kombinēšanu. Nozīmīgs ir brīvprātīgais darbs, ko uzņemas, vides aktīvisti, kopienu līderi (sociāls resurss). Pašvaldības atbalsts un vietējo politiķu ieinteresētība ir svarīgs politisks resurs. Dažkārt pašvaldības nodod kopienu lietošanā zemi dārzu ierīkošanai (materiāls resurss). PPS izstrādē būtiska loma ir zinātniekiem, konsultantiem un pārrobežu sadarbībai, kas veicina zināšanu apmaiņu un mācīšanos no citu valstu piemēriem.

## Rezultāts
PPS ir pozitīva ietekme uz pārtikas sistēmām vairākos aspektos. Ražošanas ziņā tiek veicināta vietējo zemnieku un uzņēmumu darbība, atbalstīta bioloģiskā ražošana, stimulēta     pilsētu dārzkopība. Pārstrādes un izplatīšanas jomā – iesaistīti mazie un vidējie ražotāji, veidoti daudzfunkcionāli pārtikas centri, veicinātas īsās pārtikas ķēdes, dažādoti tirdzniecības kanāli. Patēriņa aspektā tiek uzlabota iedzīvotāju piekļuve vietējai, svaigai produkcijai, uzlabota ēdināšanas kvalitāte skolās, veikti izglītojoši pasākumi par veselīgu uzturu, ieviesti risinājumi pārtikas atkritumu mazināšanai. PPS ir arī pozitīva ietekme uz sociālo saliedētību pilsētu kopienās un solidāru ekonomisko saišu veidošanu starp pilsētām un laukiem. Būtiski ir arī ekoloģiskie ieguvumi, kas izriet no transportēšanas attālumu samazināšanās.

## Secinājumi
Lai PPS izstrāde un ieviešana būtu sekmīga, nepieciešama aktīvas zināšanu un inovāciju partnerības izveidošana, tās darbība un evolūcija. Ja partnerības loks netiek paplašināts, pastāv risks, ka PPS var palikt deklaratīvu ieceru līmenī. Iesaistītajiem dalībniekiem nereti ir atšķirīgi redzējumi par PPS mērķiem un rīcībām. Interešu salāgošana ir nozīmīgs panākumu faktors. Zinātnieki un nevalstiskās organizācijas var būt noderīgi partneri zemniekiem un pārtikas ražotājiem viņu interešu virzīšanā vietējās un reģionālajās pārtikas sistēmās.
Ieteikumi: Ir nepieciešama zināma PPS procesa formalizācija - partnerības valdes, konsultatīvas padomes, vadības grupas izveidošana, kas pārrauga un koordinē PPS izstrādi un ieviešanu. Vadības grupai regulāri jākomunicē ar plašāko sabiedrību. Politiķiem ir jāuzņemas proaktīva loma ilgtspējīgu vietējo pārtikas sistēmu veidošanā.
 
## Avoti
- Keech, D. and D. Maye (2019) Food strategy review. ROBUST. https://rural-urban.eu/publications/robust-food-strategy-review; 
- Moragues, A.; Morgan, K.; Moschitz, H.; Neimane, I.; Nilsson, H.; Pinto, M.; Rohracher,H.; Ruiz, R.; Thuswald, M.; Tisenkopfs, T. and Halliday, J. (2013) Urban Food Strategies: the rough guide to sustainable food systems. Document developed in the framework of the FP7 project FOODLINKS (GA No. 265287)
- Eurocities. https://eurocities.eu/latest/what-can-cities-do-to-change-our-food-systems/
- Milan Urban Food Policy Pact. https://www.milanurbanfoodpolicypact.org/)

## Piemēri 

{{<section>}}
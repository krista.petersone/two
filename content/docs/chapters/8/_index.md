---
title: "Dzīvās prakses laboratorijas"
weight: 80

bookCollapseSection: true

author: "Emīls Ķīlis"
categories: "apraksti"
tags: [""]
---


# Dzīvās prakses laboratorijas inovāciju kopradei un ieviešanai

## Konteksts
Lai sekmētu pētnieku, praktiķu un citu ieinteresēto pušu veiksmīgu sadarbību inovāciju izstrādē, Eiropas Savienības finansētos pētniecības un inovāciju projektos arvien biežāk tiek veidotas  dzīvās prakses laboratorijas (DzPL). DzPL ir atvērtas inovāciju platformas, kuru mērķis ir risināt konkrētu kādas jomas praktiķu identificētu problēmu, eksperimentējot ar praktiskiem risinājumiem, tos pilnveidojot ieviešanas procesā. Šī pieeja akcentē jauninājumu kopradi un aktīvu kolektīvu darbību, kurā pētnieki līdzdarbojas konsultantu kapacitātē, kā arī dokumentē konkrētu risinājumu ieviešanas un izstrādes veiksmes un neveiksmes, kas ļautu citām DzPL mācīties no kolēģu pieredzes. DzPL darba organizācijas principi atšķiras, jo tie tiek pielāgoti apstākļiem un izvēlētās jomas specifikai, bet vienmēr balstīti uz praktiķu un pētnieku sadarbību. Baltic Studies Centre pētnieki ir bijuši iesaistīti vairākās DzPL, kuru mērķis bija risināt ar pilsētas-lauku mijattiecībām, pārtikas digitālo mārketingu un dārzkopību saistītas problēmas.

## Risinājums
Katra DzPL strādā pie konkrēta risinājuma, kura izstrādes process lielā mērā atkarīgs no iesaistīto pušu vajadzībām, pieejamajiem resursiem un institucionālā ietvara uzliktajiem ierobežojumiem. Visos gadījumos tiek iesaistīts plašs aģentu un organizāciju loks, lai nodrošinātu daudzveidīgu zināšanu bāzi un ļautu partneriem vienam no otra mācīties. AgriLink DzPL mērķis bija izveidot tiešsaistes platformu, kurā būtu apvienoti visi resursi, kas pašreiz pieejami konsultāciju pakalpojumu lietotājiem dārzkopībā, un ļautu ātri atrast viņu praktiskajām un komerciālajām vajadzībām atbilstošu risinājumu. ROBUST DzPL Tukumā tika veidota ar mērķi saglabāt un veicināt reģiona daudzveidīgo kultūras dzīvi. Tās centrālais uzdevums bija izstrādāt pirmo Tukuma novada kultūras stratēģiju. Darbs pie stratēģijas izstrādes pašvaldībai deva iespēju attīstīt efektīvāku un iekļaujošāku kultūras dzīves pārvaldības modeli, kā arī uzlabot kultūras piedāvājuma kvalitāti un pieejamību visa reģiona ietvaros. DESIRA DzPL mērķis ir izstrādāt novatorisku atbalsta sistēmu liellopu gaļas sektorā, lai ar digitālajiem rīkiem uzlabotu un paplašinātu tirgu. Piemēram, digitālā mārketinga stratēģija, kuras mērķis ir informēt patērētājus un lauksaimniekus par Latvijas liellopu gaļas īpašībām..

### Iesaistītie dalībnieki
Pētnieki (Baltic Studies Centre, LLU, DI), pašvaldības (Tukums), kultūras iestādes, lauksaimnieku organizācijas (Zemnieku Saeima), lauksaimniecības konsultanti (LLKC), Latvijas Augļkopju asociācija. Visos gadījumos DzPL iniciēja pētnieki un konkrēti prakses partneri, kas vienojās par sākotnējo problēmas uzstādījumu.

### Resursi
Nepieciešamais finansējums un apmācība tika nodrošināta projektu AgriLink, ROBUST un DESIRA ietvaros.

## Rezultāts
(i) izstrādāta mājaslapa, kas apkopo publiski pieejamus resursus par pārstrādi dārzkopības nozarē un palīdzēs lauksaimniekiem ātrāk un mērķtiecīgāk atrast tiem nepieciešamo informāciju (AgriLink); (ii) izstrādāts un apstiprināts Tukuma kultūrvides attīstības plāns 2020.-2025. gadam (ROBUST); (iii) izstrādāta digitālā mārketinga stratēģija kvalitatīvas liellopu gaļas patēriņa veicināšanai.

## Secinājumi
Veidojot DzPL komandā nepieciešams iekļaut cilvēkus, kas pārstāv dažādas ieinteresēto personu grupas, pirms tam veicot situācijas izpēti, lai novērtētu, kurus cilvēkus vai organizācijas nepieciešams procesā iesaistīt. Iekļaujoša kolektīva veidošanas politika sekmēs sadabību un zināšanu pārnesi, ļaujot partneriem mācīties vienam no otra un atrast vēlamo sadarbības formātu. Jāņem vērā, ka gludam un operatīvam koprades procesam komandā var būt nepieciešami līderi, kas nebaidās uzņemties vadību un virzīt radošo procesu. DzPL jāizvēlas problēmjautājums, kuru iespējams atrisināt, ņemot vērā pieejamos laika un finansiālos resursus, kā arī institucionālā ietvara uzliktos ierobežojumus. Vienlaikus jāizvēlas tēma, kas iesaistītajām pusēm šķiet aktuāla un spēs motivēt aktīvu rīcību, lai praksē ieviestu nepieciešamos risinājumus.

## Piemēri
{{<section>}}

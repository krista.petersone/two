---
title: "Vietējās pārtikas zīmološana"
weight: 10

bookCollapseSection: true

author: "Sandra Šūmane"
categories: "apraksti"
tags: [""]
---

# Vietējās pārtikas zīmološana

## Konteksts
Pieaugot sabiedrības apziņai par pārtikas ražošanas, izplatīšanas un patēriņa sociālo, vides un ekonomisko ietekmi, palielinās interese par vietējo pārtiku. Tā tiek uzlūkota kā ilgtspējīgāka alternatīva industriāli ražotai un konvencionālajās pārtikas ķēdēs izplatītai pārtikai. Patērētāju vidū pieaug pieprasījums pēc vietējās pārtikas. Piedāvājuma pusē veidojas jauni vietējas pārtikas izplatīšanas kanāli, paplašinās vietējo produktu klāsts, ienāk jauni tirgus dalībnieki: vietējās pārtikas ražošanā specializējušies lauksaimnieki, tirgus starpnieki, profesionāli pircēji (caur pārtikas iepirkumu, lielveikali). Vienlaikus daudzviet vietējiem ražotājiem, jo īpaši maziem un vidējiem, arvien ir sarežģīti piekļūt tirgum un vietējiem patērētājiem, arī dēļ negodīgiem konkurentiem, kuri piedāvā viltus vietējo pārtiku tirgū, maldinot pircējus.

## Risinājums
Vietējās pārtikas zīmološana kopienā ir viens no risinājumiem, kā veicināt autentiskas vietējās pārtikas mārketingu. Vietējās pārtikas zīmols ir komunikācijas rīks starp tirgus dalībniekiem par noteiktām produkta īpašībām. Tādējādi tas var aizstāt vai pastiprināt tiešo, uzticībā balstītu tirgus komunikāciju starp ražotāju un pircēju. Zīmolot var gan produktus, gan pakalpojumus (piemēram, ēdināšanas uzņēmumi, kuros tiek piedāvātas no vietējiem produktiem gatavotas maltītes.)
Zīmološanas procesā ir nošķirami vairāki soļi:
- Vienošanās par saturu, ko vietējās pārtikas zīmols vēstīs. Tas lielā mērā saistīts ar jautājumu, kā definēt vietējo pārtiku. Cilvēkiem var būt ļoti dažādas izpratnes, kas ir vietējā pārtika: piemēram, vai vietējā pārtika ir visa pārtika, kas ražota vietējā apkaimē; vai tai jābūt saistītai ar kādiem vietējiem resursiem (izejvielas, šķirnes, zināšanas, tradīcijas, bioloģiskā daudzveidība u.c.); vai tai jābūt ražotai vietējai videi draudzīgā veidā u.c.?
- Kopīga kvalitātes zīmola izveide un kolektīvas iniciatīvas tā atpazīstamības veicināšanai tirgū. Šeit liela loma ir ārējai komunikācijai un zīmola labas reputācijas izveidei.  Komunikācijai jāsniedz skaidrs priekšstats par produkta izcelsmi un kvalitātes īpašībām.
- Zīmola kvalitātes garantijas sistēmas izveide. Atbilstoši kopīgi definētajam zīmola saturam nepieciešams vienoties, kas un kā piešķir un uzrauga zīmola lietošanu. Bieži tas notiek caur produktu vai pakalpojumu sertifikāciju, kas var izpausties pašam ražotājam vai uzņēmējam garantējot kvalitāti, asociācijai garantējot savu biedru produktu vai pakalpojumu kvalitāti, vai izmantojot neatkarīga sertificētāja pakalpojumus.
 - Zīmola sekmīgai virzīšanai tirgū svarīga uzņēmējdarbības stratēģija un biznesa modeļa izstrāde.

Vietējās pārtikas zīmola skaidri definēti noteikumi var pildīt dažādas funkcijas. Tie var palīdzēt: 1) risināt negodīgu tirgus konkurenci 2) veicināt kopēju izpratni par vietējo pārtiku ražotāju, patērētāju un citu tirgus dalībnieku vidū; 3) komunicēt noteiktas produkta īpašības  patērētājiem; 4) regulēt kopējās kvalitātes zīmes lietošanas tiesības un piekļuvi tirgus kanāliem (piemēram, publiskajam iepirkumam, zemnieku tirgiem).

### Dalībnieki
Vietējās pārtikas zīmološanas iniciatori mēdz būt dažādi. Bieži tie ir vietējie ražotāji, taču ir svarīgi, lai zīmola izveidošanā, tā noteikumu formulēšanā būtu iesaistītas visas ieinteresētās puses – vietējie ražotāji, patērētāji, vietējās attīstības aktīvisti u.c. un lai tas būtu līdzdalības process. Līdzšinējie piemēri liecina, ka vietējās pārtikas zīmološanas procesā liela loma ir vietējām pašvaldībām un reģionu pārvaldēm dažādu iesaistīto pušu koordinēšanai un diskusiju un sadarbības veicināšanai, arī zīmola lietošanas uzraudzībai. Zīmola sekmīgai komercializēšanai svarīga ir stratēģiskas un saimnieciskas vadības izveide.

### Resursi
Ar vietējās pārtikas ražošanu saistīti resursi, piemēram, tipiskas audzēšanas un pārstrādes metodes, vietējas izejvielas, šķirnes, produkti, kā arī zināšanas un prasmes šos vietējos resursus likt lietā. Vietējo produktu zīmola veicināšanai un kontrolei tirgū (zīmola vizuālā izstrāde, mārketinga kampaņa, specifiskas tirdzniecības vietas izveide, neatkarīga sertificēšana) ir nepieciešami finanšu resursi.

## Rezultāti
Ir novērotas vairākas vietējo produktu zīmološanas pozitīvas ietekmes uz teritoriālo attīstību, savijot pārtiku, ekonomiskās aktivitātes (pārtikas uzņēmumi, tūrisms u.c.) un dabas un kultūras ainavas. Zīmološana var veicināt jaunas un stiprināt esošās vietējās un reģionālās pārtikas ķēdes caur caurspīdīgāku tirgus komunikāciju, uzlabojot vietējo ražotāju piekļuvi tirgum un palielinot viņu ienākumus, stiprinot citu pārtikas ķēžu dalībnieku (tirgotāju, ēdinātāju), kuri izmanto vietējos produktus, tirgus pozīciju. Populāri vietējo produktu zīmoli var veicināt reģionu pievilcību un tūrisma aktivitātes, sekmējot ciešākas saiknes arī starp pilsētu un lauku iedzīvotājiem. Tie stiprina arī vietējo identitāti un piederības sajūtu vietai.

## Secinājumi
Vietējās pārtikas zīmološana ir veids, kā veicināt diskusiju un sadarbību vietējo pārtikas sistēmas dalībnieku vidū vietējās teritorijas ilgtspējīgai attīstībai, liekot lietā (iespējams, jaunatklājot mazāk apzinātus) ar pārtikas ražošanu saistītus vietējos resursus, piemēram, izjevielas, zināšanas, prasmes, kultūras tradīcijas, agrobioloģisko daudzveidību u.c. Kopienas zīmološanā (atšķirībā no individuālas zīmološanas) svarīga ir iesaistīto pušu organizācija un pārvaldības sistēmas izveide zīmola lietošanas vadībai un uzraudzībai.
 
## Avoti
- ROBUST: Webinar on local food branding: https://rural-urban.eu/CoP/sustainable-food-systems
- ROBUST: Henke, R., (2020) Rural-Urban Business Model Profile: Regional Quality Labels. https://rural-urban.eu/sites/default/files/Regional%20quality%20labels.pdf

## Piemēri
{{<section>}}
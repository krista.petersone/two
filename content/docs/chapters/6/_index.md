---
title: "Demonstrējumi"
weight: 60

bookCollapseSection: true

author: "Anda Ādamsone-Fiskoviča"
categories: "apraksti"
tags: [""]
---

# Demonstrējumi kā zināšanu pārneses metode lauksaimniecībā

## Situācija
Lauksaimnieciskā ražošana prasa plašas un daudzveidīgas gan teorētiskās, gan praktiskās zināšanas par audzējamām kultūrām un mājlopu šķirnēm, saimniekošanas sistēmām, kā arī par vides apstākļiem un tehnoloģijām. Šīs zināšanas ļauj uzlabot lauksaimniecības produktu primārās ražošanas efektivitāti un pārtikas ilgtspēju. Tomēr ne visi lauksaimnieki var viegli piekļūt nepieciešamājām zināšanām un tās pielietot, jo pieejamības, pasniegšanas formāta un satura ziņā tās var būt slikti pielāgotas praktiķu vajadzībām. Ne vienmēr arī iespējams individuāli veidot kontaktus un tiešā veidā sazināties ar nozares speciālistiem un citu saimniecību pārstāvjiem, lai apspriestu kādus aktuālus saimniekošanas jautājumus.

## Risinājums
Arvien plašāku pielietojumu lauksaimniecisko zināšanu komunikācijā kā pasaulē, tā Latvijā gūst demonstrējumi, kas paredz uzskatāmu un praktisku šo zināšanu nodošanu lietotājiem. Latvijā ierasts publiski īstenoto demonstrējumu formāts ir Lauku/Fermu dienas saimniecībās, kas ietver priekšlasījumus iekštelpās un demonstrējuma objektu apskati dabā. Latvijā demonstrējumi tiek īstenoti dažādās lauksaimniecības nozarēs, aptverot gan augkopību, gan lopkopību. Šie demonstrējumi attiecināmi gan uz konvencionālo, gan integrēto un bioloģisko lauksaimniecību. Vairums demonstrējumu vērsti uz saimniekošanas efektivitātes un ražīguma paaugstināšanu, skarot arī vides ilgtspējas jautājumus. Demonstrējumu elementi sastopami arī tādos neformālākos formātos kā kooperatīva biedru saimiecību apmeklējumi vai nelielas zemnieku interešu grupas vietējā konsultanta vadībā, kas veicina lauksaimnieku savstarpēju mācīšanos. Formālie demonstrējumi var balstīties kā uz zinātnieku veiktiem izmēģinājumiem, tā izmēģinājumiem, kas īstenoti, sadarbojoties zinātniekiem, konsultantiem un lauksaimniekiem.

Attēls...

### Iesaistītās puses
Formālos demonstrējumus Latvijā lielākoties organizē valsts zinātniskie institūti (piem., Stendes un Priekuļu pētniecības centri, Dārzkopības institūts) un lauksaimniecības konsultāciju organizācijas (piem., Latvijas Lauku konsultāciju un izglītības centrs), tostarp piesaistot vietējās komercsaimniecības. Demonstrējumus atbalsta arī atsevišķas nevalstiskās organizācijas (piem., Latvijas Dabas fonds) un ražotāju kooperatīvi (piem., LATRAPS). Lauku/Fermu dienas apmeklē ne tikai lauksaimnieki, bet arī konsultanti, speciālisti (agronomi, veterinārārsti), studenti un citi interesenti.   

### Resursi
Būtiski resursi izmēģinājuma un demonstrējuma īstenošanā ir akadēmiskās un praktiskās zināšanas, kā arī pieejamā infrastruktūra un cilvēkresursi. Finansējums pamatā nāk no valsts vai starptautiskiem finanšu avotiem un bieži tiek nodrošināts projektu veidā.

## Rezultāts
Demonstrējumu formātā komunicētā informācija ir ļoti praktiska un viegli uztverama. Klātienes dalība ļauj uzskatāmā veidā redzēt reālos saimniekošanas apstākļos īstenotus risinājumus un tūlītēji tos apspriest domubiedru grupā. Formālu un neformālu sarunu gaitā demonstrējumus pasākuma laikā tiek veicināta mācīšanās gan no kolēģiem, gan no klātesošajiem citu jomu pārstāvjiem. Demonstrējuma pasākumā zināšanas gūst arī paši izmēģinājuma saimniecības pārstāvji, kuri aktīvi iesaistās domu un pieredzes apmaiņā ar dalībniekiem. Klātienē apmeklējot saimniecību, dalībnieki mēdz pamanīt un apspriest jaunus, nereti pašu saimnieku ieviestus radošus risinājumus un saimniekošanas knifus, kas var lieti noderēt. Ne mazāk svarīgs ieguvums apmeklētājiem ir iespēja uzzināt par vispārējām aktualitātēm lauksaimniecības vidē, kā arī uzturēt esošos un dibināt jaunus kontaktus ar citiem zemniekiem un speciālistiem. Attiecību veidošana un informācija par to, pie kā vērsties dažādos jautājumos, ir nozīmīgs papildinājums paša zināšanām. Savukārt sadarbībā starp pētniekiem, konsultantiem un lauksaimniekiem balstīti izmēģinājumi un demonstrējumi ļauj veidot ciešāku sasaisti starp zinātnisko izpēti un lauksaimnieku aktuālajām vajadzībām, veicinot to orientāciju uz lietotāju. 

## Secinājumi un ieteikumi
Demonstrējumi ir vērtīga iespēja lauksaimniekiem ne tikai gūt jaunas zināšanas un mācīties citam no cita, bet arī uzturēt un dibināt jaunus kontaktus, dot sev atelpas brīdi no ikdienas darbu slodzes un rutīnas. Publiski demonstrējumi lielākām grupām ir svarīgs mācīšanās formāts, bet tas nevar aizstāt individuālu pieredzes un zināšanu apmaiņu neformālā vidē un šaurākā lokā. Atsevišķs demonstrējums lielākoties var tikai rosināt uz jauninājuma ieviešanas apsvēršanu savā saimniecībā, bet nevar garantēt tā nekavējošu izplatību. Svarīgas ir arī konkrētam demonstrējumam sekojošas aktivitātes – apmācības, individuālas konsultācijas, papildu informācijas avotu nodrošināšana.

## Avoti
- Ādamsone-Fiskoviča, A., Grīviņš, M., Ķīlis, E. (2019) Demonstrējumi lopkopībā – mācīšanās un kontaktu veidošanas iespējas. Latvijas Lopkopis. http://llkc.lv/lv/nozares/lopkopiba/demonstrejumi-lopkopiba-macisanas-un-kontaktu-veidosanas-iespejas
PLAID projekta prakses anotācijas (https://ec.europa.eu/eip/agriculture/en/find-connect/projects/peer-peer-learningaccessing-innovation-through):
PA16 - Lauksaimnieciskie demonstrējumi Latvijā; 
PA24: Lauksaimniecisko demonstrējumu veiksmes faktori;
PA27 - Neformālu lauksaimniecisko demonstrējumu loma: Latvijas augļkopju kooperatīva piemērs; 
PA42: Lopkopības demonstrējumi Latvijā.
- Adamsone-Fiskovica, A., Grivins, M., Burton, R. J. F., Elzen, B., Flanigan, S., Frick, R., Hardy, C. (2021) Disentangling critical success factors and principles of on-farm agricultural demonstration events. The Journal of Agricultural Education and Extension. https://doi.org/10.1080/1389224X.2020.1844768

## Piemēri
{{<section>}}
---
title: "Virtuālie demonstrējumi lauksaimniecībā"
weight: 61

author: "Anda Ādamsone-Fiskoviča"
categories: "piemēri"
tags: [""]
---

# Virtuālie demonstrējumi lauksaimniecībā

## Situācija
Lauksaimniekiem un ar šo jomu saistītajiem speciālistiem ir pieejami dažādi informācijas avoti, kas ļauj apgūt jaunas teorētiskās un praktiskās zināšanas - tostarp profesionālās izglītības programmas, kursi un semināri, profesionālā un populārzinātniskā literatūra, pieredzes apmaiņas braucieni. Pētījumi rāda, ka visefektīvākie ir praktiski demonstrējumi, kas lauksaimniecībā nereti tiek īstenoti ar dažādu klātienes pasākumu palīdzību, piemēram, publiskām Lauku/Fermu dienām izmēģinājumu saimniecībās. Tomēr ne vienmēr interesentiem ir iespējas tos apmeklēt dēļ attāluma vai citiem loģistikas un laika ierobežojumiem.

## Risinājums
Pieaugot digitālo rīku (t. sk. viedtelefonu) lietojumam, informācijas un komunikācija tehnoloģiju un interneta pieejamībai, ir iespējams paplašināt gan zināšanu ieguves veidus, gan uzrunāto auditoriju. Arvien lielāku lomu lauksaimnieku izglītībā, apmācībā un konsultēšanā ieņem videoformāta materiāli. Daudziem lauksaimniekiem ir pašiem savi YouTube videokanāli, kas gūst plašu atsaucību kolēģu vidū. Virtuālie demonstrējumi, izmantojot lauksaimniekiem domātus vai lauksaimnieku pašu veidotus īsākus (2-5 minūšu) vai garākus videomateriālus, ir labs veids, kā dalīties ar saimniecībā izmantotām inovatīvām pieejām un risinājumiem. Tiešsaistes formāts vienlaikus paplašina gan demonstrējumu īstenošanas iespējas ražojošās saimniecībās, gan demonstrējumu pieejamību, tostarp mazina ierobežojumus, kas saistīti ar klātienes apmeklējumiem nepieciešamo laika patēriņu. Tā vietā, lai lauksaimnieki pavadītu vairākas stundas ceļā uz kādu klātienes demonstrējumu, kas beigās var izrādīties tiem no satura viedokļa mazvērtīgs, šādi virtuālie demonstrējumi var kalpot kā labs vizuāli uzskatāms sagatavošanās materiāls, lai izsvērtu demonstrējamās tehnoloģijas vai saimniekošanas pieejas atbilstību konkrētā lauksaimnieka vajadzībām un saimniecības apstākļiem. Savukārt atbilstības gadījumā video ir labs rosinājums apmeklēt klātienes demonstrējumu papildu infomācijas iegūšanai.

Attēli

Virtuālie demonstrējumi ļauj plaši izmantot gan standarta, gan 360o kameras, kā arī dronu piedāvātās iespējas, kas sniedz daudzveidīgāku vizuālo un telpisko priekšstatu par demonstrējumā aplūkojamo jautājumu. Piemēram, tās var būt virtuālās tūres, izmantojot virtuālās realitātes brilles, kas ļauj brīvi pašam skatītājam mainīt redzesleņķi un apskates objektus. Var tikt veidoti arī virtuālo saimniecību animēti modeļi, kas piedāvā iedomātas tipiskas saimniecības vides simulāciju, īstenojot virtuālu pastaigu pa to. Tās laikā iespējams aktivizēt arī dažādus videomateriālus par saimniecībā īstenotiem saimniekošanas jaunievedumiem, šos materiālus pēc nepieciešamības pielāgojot dažādām mērķauditorijām un tēmām. Šādi vizuālie līdzekļi palīdz arī pārvarēt valodas barjeras plašāku daudznacionālu auditoriju gadījumā. Video izplatīšanai var izmantot tādus sociālos medijus kā Twitter, FaceBook un YouTube, kā arī dažādus reģionāla un nacionāla mēroga lauksaimnieku un pētnieku tīklus. 

### Iesaistītās puses
Virtuālos demonstrējumus var īstenot gan individuāli, gan radošās komandās, veidojot sadarbību ar dažādu jomu pārstāvjiem, tostarp piesaistot publisko vai privāto finansējumu. Piemēram, Eiropa Savienības finansētajā projektā PLAID izveidotā virtuālās saimniecības lietotne tika izstrādāta sadarbībā ar britu universitātes spēļu tehnoloģiju programmas studentiem un zinātniskā institūta pētniekiem, izstrādes procesā konsultējoties ar potenciālajiem lietotājiem. Savukārt video materiālu izstrāde notika kopdarbībā starp lauksaimniecības konsultantiem, lauksaimniekiem un komunikācijas speciālistiem.  

### Resursi 
Virtuālo demonstrējumu īstenošanai nepieciešamie resursi var atšķirties atkarībā no izmantojamās tehnoloģijas un sarežģītības (piem., 2D, 3D). Īsu videoklipu filmēšanu veicot ar  ikdienā pieejamu viedtelefonu un izmantojot kādu no brīvpieejas skaņas un attēla apstrādes programmām, ieguldījumi būs mazāki. Savukārt profesionālāku tehnoloģiju (dronu,  360o kameru) un sarežģītāku risinājumu (virtuālo tūru, kas sastāv no videomateriālu kopuma) gadījumā tie būs lielāki, kā arī var prasīt pamatīgākas zināšanas un prasmes.
Rezultāts: Virtuālie demonstrējumi un to pieejamība dažādās platformās (viedtelefonos, planšetēs, datoros) sniedz iespēju viegli piekļūt vizuāli viegli uztveramai informācijai par inovācijām ražojošās saimniecībās, kas paplašina dažādu jauninājumu izplatību lauksaimniecībā. Šie demonstrējumi sniedz piekļuvi demonstrējumiem plašākām un daudzveidīgākām sociāldemogrāfiskām lauksaimnieku grupām gan dzimuma, gan vecuma, kā arī saimniecības lieluma un atrašanās vietas ziņā, kā arī studentiem apmācību procesā un dažādām ar lauksaimniecības praksi nesaistītām iedzīvotāju grupām.

## Secinājumi
Virtuālie demonstrējumi, izmantojot video un audiovizuālos risinājumus, ir noderīgs veids, kā dalīties ar zināšanām un veicināt dažādu jaunievedumu izplatību lauksaimnieku vidū. Praktisku demonstrējumu formāts palīdz mazināt dažādus šķēršļus gan attiecībā uz to, kā zināšanas tiek nodotas, gan kā tās tiek apgūtas. Svarīgi gan norādīt, ka virtuālie demonstrējumi drīzāk izmantojami kā klātienes demonstrējumu papildinājums, nevis to aizstājējs. Lai veicinātu kvalitatīvu videoformāta demonstrējumu pieejamību, nepieciešams arī sniegt metodisku un tehnoloģisku atbalstu lauksaimniecisko demonstrējumu īstenotājiem dažādas inovācijas atspoguļojošo video uzņemšanā, rediģēšanā un izplatīšanā.

## Avoti
- Hardy, C., Vanev, D., Alfoldi, T., Tippin, L.  (LEAF). 2019. Good Practice guidelines for Virtual Demonstration. Deliverable of PLAID project. https://plaid-h2020.hutton.ac.uk/sites/www.plaid-h2020.eu/files/PLAID_WP4_HUT_DV_Good%20Practice%20guidelines%20for%20Virtual%20Demonstrations%2027_2_19%20(003).pdf  
- Hardy, C., Sutherland, L-A. 2019.  The Virtual Farm as an alternative or an addition to ‘live on-farm demonstration’: Challenges and opportunities. Proceedings of the 24th European Seminar on Extension (and) Education (ESEE2019), pp. 227-237. https://www.crea.gov.it/documents/68457/0/European+Seminar+on+Extension+and+Education+%285%29.pdf/0aa24566-c206-67ad-cdda-aead0c5e57c8?t=1606900561856
https://ec.europa.eu/eip/agriculture/sites/default/files/20180427_ws_latvia_pres02_poster_session_plaid.pdf
- Virtuālās saimniecības lietotne: https://plaid-h2020.hutton.ac.uk/farm-app 

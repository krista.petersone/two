---
title: "Finansējums"
weight: 130

bookCollapseSection: true

author: "Miķelis Grīviņš"
categories: "apraksti"
tags: [""]
---

# Finansējuma loma ilgtspējīgu risinājumu veicināšanā 

## Situācija
No uzņēmumiem tiek sagaidīts, ka tie mainīsies līdzi laikam, ieviešot risinājumus, kas efektīvi izmantos modernās tehnoloģijas un būs vērsti uz ilgtspēju. Praksē gan uzņēmumiem var būt grūti atbilst šīm gaidām, jo jaunāko tehnoloģisko risinājumu ieviešana un prakšu maiņa var būt dārga un laikietilpīga un uzņēmumiem var nebūt pieejas līdzekļiem, kas ir nepieciešami pārmaiņu ieviešanai. Finansējuma piesaiste ir viena no nozīmīgākjām problēmām, ar kurām uzņēmumi saskaras, ieviešot uz ilgtspēju vērstas inovācijas, un arī biežākais iemesls, kādēļ uzņēmumi izvēlas neieviest inovācijas. Pētījumi lauksaimniecības jomā savukārt ilustrē, ka tikai nelielai daļai lauksaimnieku ir stabila piekļuve aizdevumiem, kamēr pārējie zemnieki nav spējuši iekļūt finanšu institūciju interešu lokā vai/ un viņiem nav izstrādāti viņu vajadzībām atbilstoši pakalpojumi. Tāpat pētījumi lauksaimniecības jomā rāda, ka atsevišķas lauksaimnieku grupas ir negatīvi noskaņotas pret kredītsaistību uzņemšanos.

## Risinājums
Lai nodrošinātu pārmaiņu ieviešanai vajadzīgo finansējumu, ir nepieciešams rīkoties vairākos virzienos. Banku sektors būtu mudināms pārlūkot piedāvāto pakalpojumu klāstu un pārskatīt, līdz kāda veida projektiem finansējums nonāk. Nereti finansējums nonāk līdz nelielam lokam uzņēmumu un automātiski paredz ierobežotu aktivitāšu loku. Vienlaikus, ņemot vērā, ka banku rīcību vada tirgus apsvērumi, nav iemesls cerēt, ka bankas patstāvīgi, bez ārēja pamudinājuma, pārskatīs savas prakses. Šī iemesla dēļ valsts pārvalde var ieviest risinājumus, kuri var mudināt bankas būt atvērtākas pret jauna veida projektiem. Starp šādiem atbalsta mehānismiem var minēt garantijas noteikta veida pārmaiņas veicinošiem projektiem, kas samazinātu banku risku, uzņemoties jaunus projektus. Tāpat pārvalde var investēt tādas atbalsta infrastruktūras izveidē, kas var samazināt  pārmaiņu  paredzešo inovāciju ieviešanas vai ekspluatācijas izmaksas, kas padarītu šādas inovācijas rentablākas. Galējā gadījumā caur projektiem pārvalde var atbalstīt mērķtiecīgi virzītas pārmaiņas, sedzot daļu no pārmaiņu ieviešanas izmaksām. Īpaša vērība visos gadījumos būtu veltāma uzņēmumiem, kuri nav līdz šim spējuši veiksmīgi piekļūt finansējumam vai kuru izmantotās prakses, piesaistot finansējumu, palielina riskus, kuriem šie uzņēmumi ir eksponēti. Te jānorāda, ka pārvaldei ir arī virkne citu instrumentu, kas ļauj noteiktu inovāciju virzienus padarīt finanšu institūcijām pievilcīgākus (piem., atbalsts apdrošināšanai, veidot fondus, kuri mākslīgi regulē piekļuvi kādiem uzņēmumiem svarīgiem resursiem, vietējās valūtas utt.).  

### Iesaistītie dalībnieki
Uzņēmumi ir šī risinājuma mērķgrupa - uzņēmumiem finansējums ir nepieciešams vēlamo pārmaiņu ieviešanai, un ar savu darbību tie arī ilustrē ar finansējuma piešķiršanu saistītos riskus. Bankas un nebanku kreditētāji var nodrošināt nepieciešamo finansējumu uzņēmumiem. Pārvalde ar dažādiem instrumentiem var būt iesaistīta, nodrošinot uzņēmējiem pieeju finansējumam. Vienlaikus sadarbībā ar zinātniekiem pārvalde var palīdzēt bankām uzlūkot pārmaiņas meklējošus uzņēmumus kā veiksmīgu investīciju objektu. Visbeidzot, lai gan Latvijā ir tikai neliels skaits piemēru (vismaz mums zināmu), arī nevalstiskās organizācijas var meklēt veidus, kā nodrošināt finansējumu uzņēmumiem, kuri tiecas ieviest nevalstiskā sektora atbalstītas vērtības. Piemēram, kopienas var vākt resursus, lai atbalstītu noteiktas iniciatīvas vai kļūt par iniciatīvu akcionāriem.

### Resursi
Nauda šajā gadījumā ir galvenais resurss. Visi pārējie mehānismi var tikt izmantoti, lai veicinātu piekļuvi šim resursam. Starp šiem veicinošajiem mehānismiem ir nepieciešams minēt tādus, kas samazina riskus vai palielina ieviesto pārmaiņu rentabilitāti. Šajā procesā neapšaubāmi nozīmīgu lomu spēlē politiskā vēlme iesaistīties - regulējums var itin viegli pārvirzīt svaru kausus tajā, kas ir rentabli, un tajā, kas ir riskanti. Kopienu atbalsts arī var būt nozīmīgs resurss. 

## Rezultāts
Rezultāts, veicinot finansējuma pieejamību, var būt dažāds, un galvenais ir kritiski izvērtēt, kurš tieši saņem finansējumu. Bet sekmīgi veicinot finansējuma pieejamību starp inovatīviem uzņēmumiem (vai - ar finansējumu atbalstot pārmaiņas), var iedrošināt uzņēmumus aktīvāk apsvērt iespēju mainīt ierastās prakses. 

## Secinājumi un ieteikumi
Finansējums var tikt izmantots, lai pārstrukturētu sagādes ķēdes vai ieviestu citas pārmaiņas. Pārvaldei ir kritiski jāizvērtē instrumenti, kas tai ir pieejami, lai veicinātu pārmaiņas. Gan pētnieki, gan nevalstiskais sektors var ietekmēt fiansējuma plūsmas.

## Avoti
- Clapp, J. (2014). Financialization, distance and global food politics, The Journal of Peasant Studies, Vol. 41, No. 5, 797–814, doi.org/10.1080/03066150.2013.875536.
- SINFO Rīports
- Grivins, M., Thorsøe, M.H. and D. Maye. (forthcoming). Financial subjectivities in the agricultural sector: a comparative analysis of relations between farmers and banks in Latvia, Denmark and the UK. Journal of Rural Studies.

## Piemēri
{{<section>}}